<?php

/**
 * @file
 * Documents hooks invoked by the Commerce GetMango module.
 */


/**
 * Allows modules to alter the parameters of an AIM API request immediately
 * prior to its submission to GetMango.
 *
 * @param $nvp
 *   An associative array of name-value-pairs that constitute the AIM API
 *   request; note that this array contains sensitive data in the form of API
 *   credentials and payment card data that should never be logged or retained
 *   elsewhere in the Drupal database or filesystem.
 * @param $payment_method
 *   The payment method instance array associated with this API request.
 */
function hook_commerce_getmango_request_alter($nvp, $payment_method) {
  // No example.
}
