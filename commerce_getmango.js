/**
 * @file
 * Javascript to generate GetMango token in PCI-compliant way.
 */

(function ($) {
  Drupal.behaviors.getmango = {
    attach: function (context, settings) {
      if (settings.getmango.fetched == undefined) {
        settings.getmango.fetched = true;

        // Clear the token every time the payment form is loaded. We only need the token
        // one time, as it is submitted to GetMango after a card is validated. If this
        // form reloads it's due to an error; received tokens are stored in the checkout pane.
        $('#getmango_token').val("");

        var createToken = function (cardFieldMap, responseHandler) {
          Mango.setPublicKey(settings.getmango.publicKey);

          var cardValues = {
            type: $('[id^=' + cardFieldMap.type + ']').val(),
            holdername: $('[id^=' + cardFieldMap.holdername + ']').val(),
            number: $('[id^=' + cardFieldMap.number + ']').val(),
            ccv: $('[id^=' + cardFieldMap.ccv + ']').val(),
            exp_month: $('[id^=' + cardFieldMap.exp_month + ']').val(),
            exp_year: $('[id^=' + cardFieldMap.exp_year + ']').val(),
          };

          Mango.token.create(cardValues, responseHandler);
        };

        var createCCVToken = function (responseHandler) {
          Mango.setPublicKey(settings.getmango.publicKey);

          Mango.ccv.create({
            'ccv': $('[id^=edit-commerce-payment-payment-details-getmango-ccvtoken]').val()
          }, responseHandler);
        };

        /**
         * makeResponseHandler
         * @param {type} form$
         * @param {type} errorDisplay$
         * @param {type} onError
         * @param {type} onSuccess
         * @param {type} tokenType Token type can be token or ccv token
         * @returns {Function}
         */
        var makeResponseHandler = function (form$, errorDisplay$, onError, onSuccess, tokenType) {
          return function (err, data) {
            if (err) {

              // Show the errors on the form.
              var errorHtml;
              for (var code in err.errors[0]) {
                errorHtml = err.errors[0][code] + '[Error ' + code + ']';
              }
              errorDisplay$.html($("<div id='commerce-getmango-validation-errors' class='messages error'></div>").html(errorHtml));

              onError && onError(form$);

            } else {

              // Get the token
              var token = data.uid;

              if (tokenType == undefined) {
                tokenType = 'token';
              }

              // Insert the token into the form so it gets submitted to the server.
              $('#getmango_' + tokenType).val(token);

              onSuccess && onSuccess(form$);

              // And submit.
              form$.get(0).submit(form$);

            }
          };
        };

        $('body').delegate('#edit-continue', 'click', function (event) {

          // Prevent the GetMango actions to be triggered if GetMango is not selected.
          if ($("input[value*='commerce_getmango|']").is(':checked')) {
            // Do not fetch the token if cardonfile is enabled and the customer has selected an existing card.
            if ($('.form-item-commerce-payment-payment-details-cardonfile').length) {
              // If select list enabled in card on file settings
              if ($("select[name='commerce_payment[payment_details][cardonfile]']").length
                  && $("select[name='commerce_payment[payment_details][cardonfile]'] option:selected").val() != 'new') {
                return;
              }

              // If radio buttons are enabled in card on file settings
              if ($("input[type='radio'][name='commerce_payment[payment_details][cardonfile]']").length
                  && $("input[type='radio'][name='commerce_payment[payment_details][cardonfile]']:checked").val() != 'new') {
                return;
              }
            }

            var form$ = $("#edit-continue").closest("form");
            var submitButtons$ = form$.find('.checkout-continue');

            // Prevent the form from submitting with the default action.
            if ($('#getmango_token').length && $('#getmango_token').val().length === 0) {
              event.preventDefault();
              submitButtons$.attr("disabled", "disabled");
            } else {
              return;
            }

            // Prevent duplicate submissions to GetMango from multiple clicks
            if ($(this).hasClass('auth-processing')) {
              return false;
            }
            $(this).addClass('auth-processing');

            // Show progress animated gif (needed for submitting after first error).
            $('.checkout-processing').show();

            // Disable the submit button to prevent repeated clicks.
            submitButtons$.attr("disabled", "disabled");

            // Remove error reports from the last submission
            $('#commerce-getmango-validation-errors').remove();

            var cardFields = {
              type: 'edit-commerce-payment-payment-details-credit-card-type',
              holdername: 'edit-commerce-payment-payment-details-credit-card-owner',
              number: 'edit-commerce-payment-payment-details-credit-card-number',
              exp_month: 'edit-commerce-payment-payment-details-credit-card-exp-month',
              exp_year: 'edit-commerce-payment-payment-details-credit-card-exp-year',
              ccv: 'edit-commerce-payment-payment-details-credit-card-code',
            };

            if ($('#getmango_ccvtoken').length) {
              var tokenType = 'ccvtoken';
            } else {
              var tokenType = 'token';
            }

            var responseHandler = makeResponseHandler(
                $("#edit-continue").closest("form"),
                $('div.payment-errors'),
                function (form$) {
                  // Enable the submit button to allow resubmission.
                  form$.find('.checkout-continue').removeAttr("disabled").removeClass("auth-processing");
                  submitButtons$.removeAttr('disabled').removeClass('auth-processing');
                  // Hide progress animated gif.
                  $('.checkout-processing').hide();
                },
                function (form$) {
                  var $btnTrigger = $('.form-submit.auth-processing').eq(0);
                  var trigger$ = $("<input type='hidden' />").attr('name', $btnTrigger.attr('name')).attr('value', $btnTrigger.attr('value'));
                  form$.append(trigger$);
                },
                tokenType
                );

            // If CCV Token present, then the user is trying to use an already saved card
            // We need to create a ccv token to attach to the current card instead of a new token
            if ($('#getmango_ccvtoken').length) {
              createToken(cardFields, responseHandler);
            } else {
              createCCVToken(responseHandler);
            }
          }

          // Prevent the form from submitting with the default action.
          return false;
        });

        $('#commerce-getmango-cardonfile-create-form').delegate('#edit-submit', 'click', function (event) {
          var cardFields = {
            type: 'edit-credit-card-type',
            number: 'edit-credit-card-number',
            holdername: 'edit-credit-card-owner',
            exp_month: 'edit-credit-card-exp-month',
            exp_year: 'edit-credit-card-exp-year',
            ccv: 'edit-credit-card-code',
          };

          var responseHandler = makeResponseHandler($('#commerce-getmango-cardonfile-create-form'), $('#card-errors'));

          createToken(cardFields, responseHandler);

          return false;
        });
      }
    }
  }
})(jQuery);
